# Creation of a global freshwater methane emissions map for atmospheric modelling #

This repository contains the code used to create the global freshwater methane emissions map for atmospheric modelling used in two of my papers:

* Sensitivity paper (https://doi.org/10.5194/acp-21-1717-2021)
* Optimisation paper (in prep)


The final global freshwater methane emissions map can be downloaded from https://doi.org/10.17605/OSF.IO/Q9F8P


The data used to produce this map is: 

https://www.worldwildlife.org/pages/global-lakes-and-wetlands-database

and the corresponding paper is:

https://doi.org/10.1016/j.jhydrol.2004.03.028


### How does the code run? ###

1. Download the data (this is done for you and stored in GLWD_data)
2. Transform the shapefiles to a grid using shapefile_to_raster.R (saved as glwd_grid in GLWD_data)
3. Weight the freshwater map according to methane emissions using freshwater_ems_map.py (output at https://doi.org/10.17605/OSF.IO/Q9F8P)

### Contact ###
If you have any questions, feel free to contact us at Matt.Rigby@bristol.ac.uk.

