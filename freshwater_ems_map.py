#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script makes a freshwater emissions map.

Author:
    Angharad Stell
"""
from datetime import date

import cartopy
from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr

# =============================================================================
# Global constants
# =============================================================================

# Make matplotlib look prettier
plt.style.use('seaborn-dark')
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = ['DejaVu Sans']
plt.rcParams['font.size'] = 14

# These are the MOZART latitude and longitude dimensions (midpoint of grid cell)
LAT_MZT = np.linspace(-90, 90, 96, dtype=np.float32)
LON_MZT = np.linspace(0, 357.5, 144, dtype=np.float32)


# =============================================================================
# Functions
# =============================================================================

def colormesh_lon(lon):
    """ Function to make lon be corners for pcolormesh plotting,
        pcolormesh wants lower left and lower right corners.
    
    Args:
        lon (1d arr): the MOZART lon
    """
    plt_lon = np.linspace(0., 360., (len(lon) + 1))
    return plt_lon


def colormesh_lat(lat):
    """ Function to make lat be corners for pcolormesh plotting,
    pcolormesh wants lower left and lower right corners.
    
    Args:
        lat (1d arr): the MOZART lat
    """
    plt_lat = (lat[1:] + lat[:-1]) / 2
    plt_lat = np.insert(plt_lat, 0, -90)
    plt_lat = np.insert(plt_lat, len(plt_lat), 90)
    return plt_lat


def plot_emissions(ems_grid):
    """ Plots the emissions map. """
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, projection=cartopy.crs.PlateCarree())

    cs = plt.pcolormesh(colormesh_lon(ems_grid["lon"].values), colormesh_lat(ems_grid["lat"].values),
                        ems_grid["ch4emissions"], transform=cartopy.crs.PlateCarree(), cmap="Reds",
                        norm=colors.LogNorm())

    cbar = plt.colorbar(cs, orientation="horizontal", pad=0.1)
    cbar.ax.tick_params(labelsize=14)
    cbar.set_label(r'Methane flux / Tg yr$^{-1}$')

    ax.coastlines()

    plt.savefig("emissions_field.pdf", bbox_inches="tight")
    plt.close()


# =============================================================================
# Code to run
# =============================================================================

if __name__ == "__main__":
    # Read in Processed Data
    glwd_grid = xr.open_dataset("GLWD_data/glwd_grid.nc")
    glwd_grid = glwd_grid.transpose('lat', 'lon').sortby('lat').sortby('lon')

    # Add up all the freshwater sources
    glwd_grid["total"] = glwd_grid["level_1_lake"] + glwd_grid["level_1_reservoir"] + \
                         glwd_grid["level_2_lake"] + glwd_grid["level_2_lake"] + glwd_grid["level_2_river"]

    glwd_grid["ch4emissions"] = glwd_grid["total"].copy()
    # Split into bands - flux not just area dependent!
    tropic = glwd_grid["ch4emissions"][abs(glwd_grid["lat"]) < 30]
    midlat = glwd_grid["ch4emissions"][np.logical_and(abs(glwd_grid["lat"]) >= 30, abs(glwd_grid["lat"]) <= 50)]
    hilat = glwd_grid["ch4emissions"][abs(glwd_grid["lat"]) > 50]

    # Global freshwater ems = 120 Tgyr-1
    tot_ems = 120

    # Turn into fraction of global freshwater
    glwd_grid["ch4emissions"][abs(glwd_grid["lat"]) < 30] = tot_ems * 0.49 * tropic / tropic.sum()
    glwd_grid["ch4emissions"][np.logical_and(abs(glwd_grid["lat"]) >= 30,
                                             abs(glwd_grid["lat"]) <= 50)] = tot_ems * 0.33 * midlat / midlat.sum()
    glwd_grid["ch4emissions"][abs(glwd_grid["lat"]) > 50] = tot_ems * 0.18 * hilat / hilat.sum()

    # Plot figure to check this map makes sense
    plot_emissions(glwd_grid)

    # I then convert to MOZART units (molecules/cm2/s), and include the first and last day of the month, and the first
    # month of the following year as MOZART takes the emissions from the nearest date. I've left these out as they're
    # specific for running MOZART. Contact me if you want them!

    # Add some useful attributes
    glwd_grid["ch4emissions"].attrs["units"] = "CH4 Tg year-1"
    glwd_grid["ch4emissions"].attrs["history"] = "created by Angharad Stell " + date.today().strftime("%d/%m/%y")
    glwd_grid["ch4emissions"].attrs["source"] = "https://osf.io/q9f8p/"
    glwd_grid["ch4emissions"].attrs["DOI"] = "10.17605/OSF.IO/Q9F8P"

    # Save file
    # MOZART takes NETCDF3_CLASSIC only, again this has been left out as it is MOZART specific
    glwd_grid["ch4emissions"].to_netcdf("freshwater_ch4_ems.nc")
